<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{   //Yii::app()->session->destroy();
		if(!Yii::app()->user->isGuest) {
			$this->redirect(['/cabinet/index']);
		}

		$model=new LoginForm;
		$block_for_login = false;

		if(Yii::app()->user->getState("count_of_attempts") && Yii::app()->user->getState("time"));
		{
			if (Yii::app()->user->getState("count_of_attempts") >= 3 && (Yii::app()->user->getState("time") + 300) > time("s")) {
				$block_for_login = true;
			}

			if (Yii::app()->user->getState("count_of_attempts") >= 3 && (Yii::app()->user->getState("time") + 300) < time("s")) {

				Yii::app()->user->setState("count_of_attempts", null);
				Yii::app()->user->setState("time", null);
				$block_for_login = false;
				$this->refresh();
			}
		}

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']) && !$block_for_login)
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()) {

				Yii::app()->user->setState("count_of_attempts", null);
				Yii::app()->user->setState("time", null);

				$this->redirect(['/cabinet/index']);
			} else {

				Yii::app()->user->setState("count_of_attempts", Yii::app()->user->getState("count_of_attempts") + 1);

				if(Yii::app()->user->getState("count_of_attempts")) {
					if(Yii::app()->user->getState("count_of_attempts") >= 3) {
						Yii::app()->user->setState("time", time("s"));
						$block_for_login = true;
					}
				}

			}
		}


		// display the login form
		$this->render('login',array(
			'model'=>$model,
			'block_for_login' => $block_for_login,
		));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(['/site/login']);
	}
}