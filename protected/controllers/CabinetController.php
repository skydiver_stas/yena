<?php

/**
 * Created by PhpStorm.
 * User: stas
 * Date: 23.06.17
 * Time: 09:22
 */
class CabinetController extends Controller
{

    protected function beforeAction($action)
    {
        if(Yii::app()->user->isGuest) {
            $this->redirect(['/site/login']);
        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $this->render('index');
    }

}